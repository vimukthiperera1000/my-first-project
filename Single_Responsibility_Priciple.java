import java.util.Scanner;

public class Single_Responsibility_Priciple {

	public void addtion(int n1, int n2) {
		System.out.println("Addtion of" +n1+ "and" +n2+ "is" +n1 + +n2);
	}
	
	public void checkVowel(char l) {
		switch(l) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
		System.out.println(l +" is Vowel");
		break;
		default:
			System.out.println(l +"is NOT a Vowel");
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n1,n2;
		System.out.println("Enter N1");
		Scanner sc=new Scanner(System.in);
		n1= sc.nextInt();
		
		System.out.println("Enter N2");
		n2= sc.nextInt();
		
		Single_Responsibility_Priciple ol = new Single_Responsibility_Priciple();
		ol.addtion(n1, n2);
		
		System.out.println("Enter latter to check Vowel");
		char l=sc.next().charAt(0);
		ol.checkVowel(l);
	}

}
